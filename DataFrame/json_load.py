import pandas as pd
from pandas.io.json import json_normalize
from flatten_json import flatten
import copy
from collections import defaultdict
import numpy as np

from pandas._libs.writers import convert_json_to_lines
from pandas import compat, DataFrame

pd.set_option("display.max_columns", 999)
# data = {
#     "data": [
#         {
#             "search": {
#                 "instId": 10,
#                 "releaseGroups": {
#                     "instId": 1,
#                     "results": [
#                         {
#                             "releaseGroup": {
#                                 "id": "b5225f7f-23e7-3e21-87c7-973f2293fa75",
#                                 "title": "Christmas! Christmas! Christmas!"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "16737576-a6d1-44df-9819-b4a6e2b2fa0a",
#                                 "title": "Christmas Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "6ac38805-61f3-3680-97af-996755a28fd7",
#                                 "title": "Christmas to Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "bc59d905-1061-4cac-8e0b-443b10151b77",
#                                 "title": "Christmas Bloody Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "329d17b1-f468-3609-b1af-2ad4e1949133",
#                                 "title": "Christmas to Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "c12978a2-0d6a-3004-ba91-fd9034764e53",
#                                 "title": "Ultraprophets of Thee Psykick Revolution"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "667a3602-727d-32f7-8fb9-2e011028d8db",
#                                 "title": "Vortex"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "f3a87152-d171-3ff1-ad7b-187025cba63a",
#                                 "title": "In Excelsior Dayglo"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "7f9822e2-8a99-471e-bbff-4d361ee27381",
#                                 "title": "Heritage"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "6e2f9f2e-d95d-449a-ad45-6b90e17bb2b9",
#                                 "title": "K Singles Zip-Pak: 2010-08-26"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "a09f73be-d520-46e6-9b36-a0a586c7df9b",
#                                 "title": "Live at Massey Hall"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "eff25c47-1f52-46e8-9fd3-446498ff9b24",
#                                 "title": "Winter"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "776d37b5-b0e4-31cc-a0b7-5ffa27113c08",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "dc0d343b-431a-3270-a5b8-3f2c5214899c",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "08eca91c-8cb6-3bb8-a782-150ec653a1a4",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "b773024b-d77b-3cef-899c-0692ac3a2763",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "c12e7b3e-e108-32b7-9cb2-e2a22d9beb66",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "d4ceccb3-4aaf-388c-b4e9-a491ab1e4a1f",
#                                 "title": "Christmas!"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "cbc97b09-4dce-39e4-b6b7-38af46cd8ddc",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "bfd44511-9a0f-3b12-88ab-c4a1c0868efe",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "873603c4-5f3a-3c49-a856-08eab4ae6c5c",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "4254b8c1-dad2-36e7-b5a0-6ebd894643cb",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "17db00ee-a7a6-31f6-8435-f70f77a363cb",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "bdbb0bf4-65d6-38cd-85f3-967c0d5b6336",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "386c492a-84a9-3e9b-811a-2992861734fb",
#                                 "title": "Christmas"
#                             }
#                         }
#                     ]
#                 }
#             }
#         },
#         {
#             "search": {
#                 "instId": 11,
#                 "releaseGroups": {
#                     "instId": 1,
#                     "results": [
#                         {
#                             "releaseGroup": {
#                                 "id": "b5225f7f-23e7-3e21-87c7-973f2293fa75",
#                                 "title": "Christmas! Christmas! Christmas!"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "16737576-a6d1-44df-9819-b4a6e2b2fa0a",
#                                 "title": "Christmas Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "6ac38805-61f3-3680-97af-996755a28fd7",
#                                 "title": "Christmas to Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "bc59d905-1061-4cac-8e0b-443b10151b77",
#                                 "title": "Christmas Bloody Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "329d17b1-f468-3609-b1af-2ad4e1949133",
#                                 "title": "Christmas to Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "c12978a2-0d6a-3004-ba91-fd9034764e53",
#                                 "title": "Ultraprophets of Thee Psykick Revolution"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "667a3602-727d-32f7-8fb9-2e011028d8db",
#                                 "title": "Vortex"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "f3a87152-d171-3ff1-ad7b-187025cba63a",
#                                 "title": "In Excelsior Dayglo"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "7f9822e2-8a99-471e-bbff-4d361ee27381",
#                                 "title": "Heritage"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "6e2f9f2e-d95d-449a-ad45-6b90e17bb2b9",
#                                 "title": "K Singles Zip-Pak: 2010-08-26"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "a09f73be-d520-46e6-9b36-a0a586c7df9b",
#                                 "title": "Live at Massey Hall"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "eff25c47-1f52-46e8-9fd3-446498ff9b24",
#                                 "title": "Winter"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "776d37b5-b0e4-31cc-a0b7-5ffa27113c08",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "dc0d343b-431a-3270-a5b8-3f2c5214899c",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "08eca91c-8cb6-3bb8-a782-150ec653a1a4",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "b773024b-d77b-3cef-899c-0692ac3a2763",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "c12e7b3e-e108-32b7-9cb2-e2a22d9beb66",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "d4ceccb3-4aaf-388c-b4e9-a491ab1e4a1f",
#                                 "title": "Christmas!"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "cbc97b09-4dce-39e4-b6b7-38af46cd8ddc",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "bfd44511-9a0f-3b12-88ab-c4a1c0868efe",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "873603c4-5f3a-3c49-a856-08eab4ae6c5c",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "4254b8c1-dad2-36e7-b5a0-6ebd894643cb",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "17db00ee-a7a6-31f6-8435-f70f77a363cb",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "bdbb0bf4-65d6-38cd-85f3-967c0d5b6336",
#                                 "title": "Christmas"
#                             }
#                         },
#                         {
#                             "releaseGroup": {
#                                 "id": "386c492a-84a9-3e9b-811a-2992861734fb",
#                                 "title": "Christmas"
#                             }
#                         }
#                     ]
#                 }
#             }
#         }
#     ]
# }

data = {
    "data": [
        {
            "account": {
                "instId": 1,
                "portfolios": [
                    {
                        "instId": 11
                    },
                    {
                        "instId": 12
                    },
                    {
                        "instId": 13
                    }
                ]
            }
        }
    ]
}


def nested_to_record(ds, new, prefix="", sep=".", level=0):
    singleton = False
    if isinstance(ds, dict):
        ds = [ds]
        singleton = True

    new_ds = []
    for d in ds:

        new_d = copy.deepcopy(d)
        for k, v in d.items():
            # each key gets renamed with prefix
            if not isinstance(k, compat.string_types):
                k = str(k)
            if level == 0:
                newkey = k
            else:
                newkey = prefix + sep + k

            # only dicts gets recurse-flattend
            # only at level>1 do we rename the rest of the keys
            if not isinstance(v, dict):
                if level != 0:  # so we skip copying for top level, common case
                    v = new_d.pop(k)
                    new_d[newkey] = v
                    continue
                elif isinstance(v, list):
                    for x in v:


            else:
                v = new_d.pop(k)
                new_d.update(nested_to_record(v, newkey, sep, level + 1))
        new_ds.append(new_d)

    if singleton:
        return new_ds[0]
    return new_ds


print(nested_to_record(data['data']))
