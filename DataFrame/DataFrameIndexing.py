import pandas as pd

# MultiIndex Example
df = pd.DataFrame.from_csv("data.csv", parse_dates=True, index_col=[0, 1])
print df

# After fact
df = pd.DataFrame.from_csv("data.csv", parse_dates=True)
df.reset_index(inplace=True)
df.set_index(['symbol', 'date'])
print df

for col in df.shape():
    