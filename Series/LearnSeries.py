import os
import sys

def verify_arith_ops (s1, s2):
    print s1 + s2

try:
    import pandas as pd
    from pandas import Series

    # Define a Series
    series = Series(xrange(1, 10), index=xrange(1, 10))
    pd_series = pd.Series(xrange(1, 10), index=xrange(11, 20))
    appened_series = series.append(pd_series, verify_integrity=True)
    print appened_series
    print pd_series
    # Length, Size
    print series.size
    print series.shape
    # count of each values
    # print appened_series.value_counts()
    # iloc - locates based on index location
    print appened_series.iloc[11]
    # loc - locates based on label location
    print appened_series.loc[11]
    # verify +

    verify_arith_ops(Series([1, 3, 5]), Series([2, 4, 6]))

except ImportError as e:
    print("Error while importing modules", e)
    sys.exit(1)
