# Copy and Paste in IPython Notebook
# The script takes CSV file in a specific format
# This script demonstrate usge of GroupBy and Pivot in Pandas's DataFrame

# Ignore warning in the notebook
import warnings

warnings.filterwarnings('ignore')
# This analysis requires Pandas, Seaborn, NumPy & MatPlotLib
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import plotly.plotly as py
import plotly.graph_objs as go
# IPython Settings
% matplotlib inline
pd.set_option('display.width', 2000)
# Setup
import plotly

plotly.offline.init_notebook_mode(connected=True)
import cufflinks as cf

cf.go_offline()
import colorlover as cl

color_scale_blues = cl.scales['9']['qual']['Paired']


def get_group_by(df, group_columns, value_columns=[]):
    """
    Weight computation for rating schemes
    """
    df_cp = df.copy(deep=True)
    cols_to_keep = group_columns + value_columns
    df_cp.drop([x for x in df_cp.columns.values if x not in cols_to_keep], axis=1, inplace=True)
    df_cp['count'] = 1
    df_cp['denom_c'] = 0
    df_cp['denom_n'] = 0
    df_cp['wt'] = 0.0
    df_grp = df_cp.groupby(group_columns).sum()
    df_grp.reset_index(inplace=True)
    cond_n = (df_grp['RATING'].isin(['1', '2', '3', '4', '5']))
    df_grp['denom_n'] = df_grp[cond_n].groupby(group_columns[:-1])['count'].transform(np.sum)
    df_grp = df_grp.fillna(0)
    df_grp['denom_t'] = df_grp['denom_n']
    df_grp.drop(['denom_n'], axis=1, inplace=True)
    df_grp['wt'] = (df_grp['count'] / df_grp['denom_t']) * 100
    return df_grp


# Load Rating CSV File
gia_df = pd.read_csv("ratings.csv")
gia_df = get_group_by(gia_df, ['SECTOR', 'ANALYST', 'RATING'])

# Create Report DataFrame
report_df = pd.DataFrame({'Sector': gia_df['SECTOR'] + " / " + gia_df['ANALYST'].
                         apply(lambda x: x.rsplit(',', 1)[0]),
                          'Ranking': gia_df['RATING'],
                          'Wt': gia_df['wt']})

# Pivot to transform Ratings into columns
report_df = report_df.pivot(index='Sector', columns='Ranking', values='Wt').fillna(0).reset_index()

report_df = report_df[['Sector', '1', '2', '3', '4', '5']]
# Plot
for x in gia_df['SECTOR'].unique():
    report_df[report_df.Sector.str.contains(x)].iplot(kind='bar',
                                                      barmode='stack',
                                                      x='Sector',
                                                      colors=color_scale_blues,
                                                      bargap=.1,
                                                      theme='pearl')
