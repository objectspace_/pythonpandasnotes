"""
Here lies my effort to conquer DataFrame - a beast I have been trying hard to tame.
DataFrame consists of multiple Series

Things I know so far -
1. DataFrame takes Series or NDArray.
   They both makes a new row in DataFrame
2. Adding column will be done based on matching index
   If index is not found then those entries will not be added
   NaN is added if the existing index labels are not found in the added series
"""
import pandas as pd
import numpy as np


# Use of Generator & yield
def get_series(args):
    """
    This function returns a generator from *args
    """
    for series in args:
        yield series


# Function to create DataFrame with *args
def create_from_series(columns, *args):
    """
    Creates a DataFrame from the passed Series
    """
    df = pd.DataFrame(get_series(args))
    df.columns = columns
    return df

def slicing(df, rows):
    print (df[:rows])

# Iterating over a DataFrame
def iterate_and_print(df, rows=None, columns=None, index=None):
    """
     Demonstrate how to iterate over a DataFrame
    """
    if index is not None:
        print (df.loc[index])

    if columns is None:
        print (df[:rows])


dataFrame = create_from_series([x for x in range(1, 5)], pd.Series(range(1, 5)), pd.Series(range(11, 15)))

# 1. Slicing
# slicing(dataFrame, 1)
# 2. Adding a column
dataFrame['5'] = pd.Series(range(20, 22))
print (dataFrame)
dataFrame.describe()