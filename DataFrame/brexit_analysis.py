import pandas as pd
import numpy as np

def merge():
    _from = 1997
    _to = 2016
    df = None
    for i in range(_from, _to):
        file = '/Users/Zuber/Code/Brexit/Data/' + '826-' + str(i) + '.csv'
        dfn = pd.DataFrame.from_csv(file)
        if df is not None:
            df = pd.DataFrame.append(df, dfn)
        else:
            df = dfn
    return df


def eu_nations():
    return ['Austria',
            'Belgium',
            'Belgium-Luxembourg'
            'Bulgaria',
            'Croatia',
            'Republic of Cyprus',
            'Czech Republic',
            'Denmark',
            'Estonia',
            'Finland',
            'France',
            'Germany',
            'Greece',
            'Hungary',
            'Ireland',
            'Italy',
            'Latvia',
            'Lithuania',
            'Luxembourg',
            'Malta',
            'Netherlands',
            'Poland',
            'Portugal',
            'Romania',
            'Slovakia',
            'Slovenia',
            'Spain',
            'Sweden']


def checkFile(fileName):
    import os
    file_name = '/Users/Zuber/Code/Brexit/' + fileName
    if os.path.exists(file_name):
        size = os.path.getsize()
        print(size)

def 


if __name__ == '__main__':
    merged_df = merge()
    nations = eu_nations()


    filtered_df = merged_df[merged_df['Partner'].isin(nations)]
    filtered_df = merged_df[merged_df['Partner'].isin(nations)]

    import_eu_df = filtered_df[filtered_df['Trade Flow'] == 'Import']
    export_eu_df = filtered_df[filtered_df['Trade Flow'] == 'Export']
    imported_items = import_eu_df['Commodity'].unique()
    exported_items = export_eu_df['Commodity'].unique()

    print(imported_items)

    print(exported_items)

    print(np.setdiff1d(exported_items, imported_items))

    # print(filtered_df)
