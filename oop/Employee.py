class Employee (object):
    """
    Class to represent Employee domain.
    It has first name, last name, age, salary attributes
    """
    def __init__(self, firstNm=None, lastNm=None, age=None, salary=None):
        """
        Constructor which by default accepts no values
        :param firstNm: First Name of the employee
        :param lastNm: Last Name of the employee
        :param age: Age of the employee
        :param salary: Salary of the employee
        """
        self._firstNm = firstNm
        self._lastNm = lastNm
        self._age = age
        self._salary = salary



e1 = Employee("Zuber", "Saiyed", 35, 148000.00)
print "Employee is %s aged %s with salary of %s" % (e1._firstNm + " " + e1._lastNm, e1._age, e1._salary)
